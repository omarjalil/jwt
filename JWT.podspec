Pod::Spec.new do |s|
  s.name         = 'JWT'
  s.version      = "1.0"
  s.license      = 'MIT'
  s.summary      = 'A JSON Web Token implementation in Objective-C.'
  s.homepage     = "https://bitbucket.org/omarjalil/jwt"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { 'Jalil' => 'omarjalilfierro@gmail.com' }
  s.source       = { :git => "https://bitbucket.org/omarjalil/jwt.git" }
  s.source_files = '*.{h,m}'
end